from django.urls import path
from .views import ProuductList,CategoryList,ProuductDetail

app_name = "blog"
urlpatterns = [
	path('',ProuductList.as_view(),name="home"),
	path('prouduct/<slug:slug>', ProuductDetail.as_view(), name="detail"),
	path('category/<slug:slug>', CategoryList.as_view(), name="category"),
	path('category/<slug:slug>/page/<int:page>', CategoryList.as_view(), name="category"),
]
