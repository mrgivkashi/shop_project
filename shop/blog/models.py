from django.db import models
from django.utils import timezone

class ProuductManager(models.Manager):
    def published(self):
        return self.filter(status='p')

class CategoryManager(models.Manager):
    def active(self):
        return self.filter(status='True')


# Create your models here.
class Category(models.Model):
    parent = models.ForeignKey('self', default=None, null=True, blank=True, on_delete=models.SET_NULL,
                               related_name='childeren', verbose_name="زیر دسته ")
    title = models.CharField(max_length=200, verbose_name="عنوان  دسته بندی")
    slug = models.SlugField(max_length=100, unique=True, verbose_name="آدرس محصول")
    status = models.BooleanField(default=True, verbose_name="ایا نمایش داده شود؟ ")
    position = models.IntegerField(verbose_name="پوزیشن")

    class Meta:
        verbose_name = "دسته بندی"
        verbose_name_plural = " دسته بندی ها "
        ordering = ['parent__id', 'position']

    def __str__(self):
        return self.title

    objects = CategoryManager()



# Create your models here.
class Prouduct(models.Model):
    STATUS_CHOICES = (
        ('d', 'پیش نویس '),
        ('p', "منتشر شده"),
    )
    name = models.CharField(max_length=200, verbose_name="نام محصول")
    price = models.CharField(max_length=200, verbose_name="قیمت ")
    slug = models.SlugField(max_length=100, unique=True, verbose_name="آدرس ")
    category = models.ManyToManyField(Category, verbose_name="دسته بندی ", related_name="prouducts")
    description = models.TextField(verbose_name="توضیحات ")
    thumbnail = models.ImageField(upload_to="images", verbose_name="تصویر محصول ")
    publish = models.DateTimeField(default=timezone.now, verbose_name=" زمان انتشار")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, verbose_name=" وضعیت")

    class Meta:
        verbose_name = "محصول "
        verbose_name_plural = "محصولات "
        ordering = ['-publish']

    def __str__(self):
        return self.name

    objects = ProuductManager()
