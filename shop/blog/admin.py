from django.contrib import admin
from .models import Prouduct, Category

admin.site.site_header = " وبلاگ "
# Register your models here.
class ProuductAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'publish', 'status', 'Category_to_str')
    list_filter = ('publish', 'status')
    search_fields = ('name', 'description')
    prepopulated_fields = {'slug': ('name',)}

    def Category_to_str(self, obj):
        return "، ".join([category.title for category in obj.category.active()])

    Category_to_str.short_description = "دسته بندی"


admin.site.register(Prouduct, ProuductAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('position', 'title', 'slug', 'parent', 'status')
    list_filter = (['status'])
    search_fields = ('title', 'slug')
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Category, CategoryAdmin)
