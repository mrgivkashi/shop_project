from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.views.generic import ListView,DetailView
from .models import Prouduct, Category
# Create your views here.

def answer(request):
    return HttpResponse("Hello World")


class ProuductList(ListView):
    template_name = "blog/index.html"
    context_object_name = "prouducts"
    queryset = Prouduct.objects.published()

class ProuductDetail(DetailView):
    def get_object(self):
        slug = self.kwargs.get('slug')
        return get_object_or_404(Prouduct.objects.published(), slug=slug)


class CategoryList(ListView):
    template_name = 'blog/index.html'
    context_object_name = "prouducts"
    def get_queryset(self):
        global category
        slug = self.kwargs.get('slug')
        category = get_object_or_404(Category.objects.active(), slug=slug)
        return category.prouducts.published()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = category
        return context

